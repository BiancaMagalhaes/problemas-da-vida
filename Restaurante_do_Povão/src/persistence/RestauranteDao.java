package persistence;

import java.util.ArrayList;
import java.util.List;


import domain.Alimentos;
import domain.Mesas;
import enums.MesasStatus;
import tui.Sistema;

public class RestauranteDao {
	
	static List<Alimentos> cardapio = new ArrayList<Alimentos>();
	static List<Mesas> mesas = new ArrayList<Mesas>();
	

	public static void listaAlimentos() {
		System.out.println("******* Cardapio ********");
		for (int i = 0; i < cardapio.size(); i++) {
			System.out.println("Alimento: " + cardapio.get(i).getNome());
			System.out.println("Descri��o: " + cardapio.get(i).getDescricao());
			System.out.println("Valor: " + cardapio.get(i).getValor());
			System.out.println("Codigo: " + cardapio.get(i).getCodigo());
			System.out.println("-----------------------------------------------------");
		}
		
	}

	public static void conta(int codigo, int numero) {
		for (int i = 0; i < cardapio.size(); i++) {
			if(cardapio.get(i).getCodigo() == codigo) {
				for (int j = 0; j < mesas.size(); j++) {
					if(mesas.get(j).getNumero() == numero) {
						mesas.get(j).setConta(mesas.get(j).getConta() + cardapio.get(i).getValor());
						break;
					}
				}
				break;
			}
		
		
	}
		System.out.println("Seu pedido foi efetuado com sucesso e chegar� dentro de 50-60 min");
	}

	public static void mesasLivres() {
		System.out.println("********* Mesas Livres********");
		for (int i = 0; i < mesas.size(); i++) {
			if(mesas.get(i).getStaus().equals(MesasStatus.LIBERADA)) {
				System.out.println(mesas.get(i).getNumero());
			}
		}
		Sistema.escolherMesa();
		
		
	}

	public static void adicionarAlimento(Alimentos alimento) {
		cardapio.add(alimento);
		
	}

	public static void adicionarMesa(Mesas mesa) {
		mesas.add(mesa);
	}

	public static void statusMesa(int numero) {
	       for (int i = 0; i < mesas.size(); i++) {
			if(mesas.get(i).getNumero() == numero) {
				if(mesas.get(i).getStaus().equals(MesasStatus.LIBERADA)) {
					mesas.get(i).setStaus(MesasStatus.OCUPADA);
				}else {
					System.out.println("Esta mesa n�o esta diponivel, escolha outra");
					Sistema.escolherMesa();
					
				}
			}
		}
		
	}

	public static void fecharConta(int numero) {
	      for (int i = 0; i < mesas.size(); i++) {
			if(mesas.get(i).getNumero() == numero) {
				System.out.println("Valor: " + mesas.get(i).getConta());
				break;
			}
		}
	      
		
	}


}
