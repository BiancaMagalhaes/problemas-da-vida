package tui;


import java.util.Scanner;

import domain.Alimentos;
import domain.Mesas;
import enums.MesasStatus;
import persistence.RestauranteDao;

public class Sistema {

	static Scanner sc = new Scanner(System.in);
	static int codigo = 000000;
	
	public static void cadastroAlimento() {
		System.out.println("*******Cadastro Alimento********");
		System.out.println("Digite o nome do alimento:");
		String nome = sc.next();
		System.out.println("Descri��o do alimento:");
		String descricao = sc.next();
		System.out.println("Valor do alimento: (XX.XX)");
		double valor = sc.nextDouble();
		codigo++;
		Alimentos alimento = new Alimentos(codigo, nome, descricao, valor);
		RestauranteDao.adicionarAlimento(alimento);
		
	}

	public static void cadastroMesa() {
		System.out.println("**********Cadastro Mesa***********");
		System.out.println("Digite o numero da mesa:");
		int numero = sc.nextInt();
		System.out.println("Capacidade da Mesa: ");
		int capacidade = sc.nextInt();
		System.out.println("Quando voc� cria uma mesa a mesma possui um status de LIBERADA e uma conta com valor ZERADO");
		MesasStatus status = MesasStatus.LIBERADA;
		double conta = 0.00;
		Mesas mesa = new Mesas(numero, capacidade, status, conta);
		RestauranteDao.adicionarMesa(mesa);
	}
	
	public static void pedido() {
		System.out.println("*********Pedido**********");
		System.out.println("Por favor digite o codigo do alimento que deseja relizar o pedido:");
		int codigoAlimento = sc.nextInt();
		System.out.println("Por favor digite o numero da mesa que deseja relizar o pedido:");
		int numeroMesa = sc.nextInt();
		RestauranteDao.conta(codigoAlimento, numeroMesa);
		
	}

	public static void escolherMesa() {
		System.out.println("Digite o numero da mesa escolhida:");
		int numero = sc.nextInt();
		RestauranteDao.statusMesa(numero);
		
	}

}
