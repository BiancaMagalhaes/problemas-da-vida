package tui;

import java.util.Scanner;

public class Gerente {

	static Scanner sc = new Scanner(System.in);

	public static void cadastro() {
		System.out.println("*****Digite o numero correspondente ao que deseja cadastrar*****");
		System.out.println("(1) Alimento \n(2) Mesa");
		int cadastro = sc.nextInt();
		boolean resp = false;
		while (resp == false) {
			if (cadastro == 1) {
				Sistema.cadastroAlimento();
				resp = true;
			} else if (cadastro == 2) {
				Sistema.cadastroMesa();
				resp = true;
			} else {
				resp = false;
			}

		}
		
		
	}
}
