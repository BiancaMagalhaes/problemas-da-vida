package tui;

import java.util.Scanner;

import persistence.RestauranteDao;

public class Cliente {

	static Scanner sc = new Scanner(System.in);
	
	public static void menu() {
		System.out.println("*****Digite o numero correspondente a a��o que deseja realizar*****");
		System.out.println("(1) Ver Cardapio \n(2) Selecionar Mesa \n(3)Fazer um pedido \n(4)Pedir a conta");
		int opcao = sc.nextInt();
		boolean resp = false;
		while(resp == false) {
			switch (opcao) {
			case 1:
				RestauranteDao.listaAlimentos();
				resp = true;
			case 2:
				Sistema.escolherMesa();
				resp = true;
			case 3:
				Sistema.pedido();
				resp = true;
			case 4:
				System.out.println("*********Fechar Conta*******");
				System.out.println("Digite o numero da mesa:");
				int numero = sc.nextInt();
				RestauranteDao.fecharConta(numero);
				System.out.println("Voc� pode se dirigir ao caixar e efutuar o seu pagamento.");
				resp = true;
			}
		}
		menu();
	}

}
