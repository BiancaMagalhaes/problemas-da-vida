package domain;

import enums.MesasStatus;

public class Mesas {
	
	int numero;
	int capacidade;
	double conta;
	MesasStatus status;
	
	public Mesas (int numero, int capacidade, MesasStatus status, double conta) {
		this.numero =numero;
		this.capacidade = capacidade;
		this.status = status;
		this.conta = conta;
	}

	public double getConta() {
		return conta;
	}

	public void setConta(double conta) {
		this.conta = conta;
	}

	public MesasStatus getStaus() {
		return status;
	}

	public void setStaus(MesasStatus staus) {
		this.status = staus;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public int getCapacidade() {
		return capacidade;
	}

	public void setCapacidade(int capacidade) {
		this.capacidade = capacidade;
	}
	
	

}
