package domain;

import enums.BancoEnum;

public class Correntistas {
	
	//cadastro
    String cpf;
    String nome;
	String telefone;
	String endereco;
	double rendaM;
     int numConta;
	
	//doucumentos
	String rg;
	
	
	//conta
	double saldo;
	double credito;
	BancoEnum statusConta;
	
	
	public Correntistas(String cpf, String nome, String telefone, String endereco, double rendaM, String rg, double saldo, double credito, BancoEnum statusConta, int numConta) {
		this.cpf = cpf;
		this.nome = nome;
		this.telefone = telefone;
		this.endereco = endereco;
		this.rendaM = rendaM;
		this.rg = rg;
		this.saldo = saldo;
		this.credito = credito;
		this.statusConta =statusConta;
		this.numConta = numConta;
		
	}
	
	public int getNumConta() {
		return numConta;
	}

	public void setNumConta(int numConta) {
		this.numConta = numConta;
	}
	
	public BancoEnum getStatusConta() {
		return statusConta;
	}


	public void setStatusConta(BancoEnum statusConta) {
		this.statusConta = statusConta;
	}


	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public double getRendaM() {
		return rendaM;
	}
	public void setRendaM(double rendaM) {
		this.rendaM = rendaM;
	}
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	public double getCredito() {
		return credito;
	}
	public void setCredito(double credito) {
		this.credito = credito;
	}

}
