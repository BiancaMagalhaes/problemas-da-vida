package persistence;

import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import domain.Correntistas;
import enums.BancoEnum;
import tui.BancoTUI;
import tui.Sistema;

public class BancoDAO {

	 static List<Correntistas> correntista = new ArrayList<Correntistas>();
	 static ImageIcon icon2 = new ImageIcon();

	public static void criarConta(Correntistas conta) {
		correntista.add(conta);
	}

	public static Correntistas buscarConta(String nome) {
		int cont = 0, num, j = 0;
		String txt = "";
		
		for (int i = 0; i < correntista.size(); i++) {
			correntista.get(i);
			System.out.println("Correntista: " + correntista.get(i).getNome() + " i: " + i + "Telefone " + correntista.get(i).getTelefone() + "\nNumero Conta: " + correntista.get(i).getNumConta() );
			correntista.get(i);
			if (correntista.get(i).getNome().equals(nome)) {
				txt += correntista.get(i).getNumConta() + " e ";
				cont++;
				System.out.println(cont);
			} 
		}
		if (cont > 1) {
			System.out.println("kkk");
			num = Integer.parseInt(JOptionPane.showInputDialog(icon2, "Escolha uma das suas contas " + txt + ":"));
			for (int i = 0; i < correntista.size(); i++) {
				if (correntista.get(i).getNumConta() == num) {
					BancoTUI.conta(correntista.get(i));
					break;
				} else if (i == correntista.size() - 1) {
					JOptionPane.showMessageDialog(null, "Voc� digitou o numero da conta errado!", "Banco do Pov�o",
							JOptionPane.INFORMATION_MESSAGE, icon2);
					buscarConta(nome);
				}
			}
		} else if (cont == 1) {
			j = j - 1;
			BancoTUI.conta(correntista.get(j));
		} else {
			JOptionPane.showMessageDialog(null, "Nome n�o consta no sistema", "Banco do Pov�o",
					JOptionPane.INFORMATION_MESSAGE, icon2);
			String [] erro = {"Sim", "Digitei errado!", "N�o"};
			int resp = JOptionPane.showOptionDialog(null,  "Deseja abrir uma conta?", "Banco do Pov�o", JOptionPane.YES_NO_OPTION,
					JOptionPane.INFORMATION_MESSAGE, icon2, erro, erro[0]);
			if (resp == 0) {
				BancoTUI.abrirConta();
			}else if(resp == 1) {
				Sistema.clienteConta();
			} else {
				JOptionPane.showMessageDialog(null, "Obrigada pela visita!", "Banco do Pov�o",
						JOptionPane.INFORMATION_MESSAGE, icon2);
				System.exit(0);
			}

		}
		return correntista.get(j);
	}

	public static void deposito(double deposito, Correntistas correntista) {
		correntista.setSaldo(correntista.getSaldo() + deposito);
		JOptionPane.showMessageDialog(null, "Seu deposito no valor de R$ " + deposito + " foi efetuado.",
				"Banco do Pov�o", JOptionPane.INFORMATION_MESSAGE, icon2);

	}

	public static void saque(Double saque, Correntistas correntista) {
		correntista.setSaldo(correntista.getSaldo() - saque);
		JOptionPane.showMessageDialog(null, "Seu saque no valor de R$ " + saque + " foi efetuado.", "Banco do Pov�o",
				JOptionPane.INFORMATION_MESSAGE, icon2);
		if (correntista.getSaldo() < 0) {
			correntista.setStatusConta(BancoEnum.BLOQUEADA);
			JOptionPane.showMessageDialog(null,
					"Por motivos de cobran�a maiores sua conta teve que ser BLOQUEADA. \n Para desbloquea-la retire o seu saldo da posi��o negativa.",
					"Banco do Pov�o", JOptionPane.INFORMATION_MESSAGE, icon2);
		}

	}

	public static void consulta(Correntistas correntista) {
		JOptionPane.showMessageDialog(null, "Seu saldo:" + correntista.getSaldo(), "Banco do Pov�o",
				JOptionPane.INFORMATION_MESSAGE, icon2);

	}

	public static void bloqueio(Correntistas correntista) {
		correntista.setStatusConta(BancoEnum.BLOQUEADA);
		JOptionPane.showMessageDialog(null, "Sua conta foi bloqueada!", "Banco do Pov�o",
				JOptionPane.INFORMATION_MESSAGE, icon2);

	}

	public static void encerramento(Correntistas correntista) {
		if(correntista.getSaldo()>0) {
			int resp = JOptionPane.showConfirmDialog(null,
					"Voc� precisa realizar o saque do valor que existe na sua conta.\n Saldo: " + correntista.getSaldo(),
					"Banco do Pov�o", JOptionPane.YES_NO_OPTION, JOptionPane.YES_NO_OPTION, icon2);
			if(resp == 0) {
				JOptionPane.showMessageDialog(null, "Seu saque no valor de R$ " + correntista.getSaldo() + " foi efetuado.", "Banco do Pov�o",
						JOptionPane.INFORMATION_MESSAGE, icon2);
				correntista.setSaldo(correntista.getSaldo() - correntista.getSaldo());
			}else
				JOptionPane.showMessageDialog(null, "Neste caso n�o ppodemos efetuar o ENCERRAMENTO da conta.", "Banco do Pov�o",
						JOptionPane.INFORMATION_MESSAGE, icon2);	
		}
		correntista.setStatusConta(BancoEnum.ENCERRADA);
		JOptionPane.showMessageDialog(null, "Sua conta foi encerrada!", "Banco do Pov�o",
				JOptionPane.INFORMATION_MESSAGE, icon2);
		int resp = JOptionPane.showConfirmDialog(null,
				"Deseja abrir uma nova conta?",
				"Banco do Pov�o", JOptionPane.YES_NO_OPTION, JOptionPane.YES_NO_OPTION, icon2);
		if(resp == 0) {
			BancoTUI.abrirConta();
		}else {
			JOptionPane.showMessageDialog(null, "Obrigada pela visita!", "Banco do Pov�o",
					JOptionPane.INFORMATION_MESSAGE, icon2);
			System.exit(0);
		}

	}

	public static void desbloqueio(Correntistas correntista) {
		if (correntista.getSaldo() >= 0) {
			correntista.setStatusConta(BancoEnum.ATIVA);
			JOptionPane.showMessageDialog(null, "Bem-vindo de volta ao Pov�o! \nSua conta est� ativa!",
					"Banco do Pov�o", JOptionPane.INFORMATION_MESSAGE, icon2);
		} else
			JOptionPane.showMessageDialog(null,
					"Desculpe n�o podemos desbloquear a sua conta por motivos maiores de cobran�as.", "Banco do Pov�o",
					JOptionPane.INFORMATION_MESSAGE, icon2);
	}

	public static void statusConta(Correntistas correntista) {
		JOptionPane.showMessageDialog(null, "Status da sua Conta: " + correntista.getStatusConta(),
				"Banco do Pov�o", JOptionPane.INFORMATION_MESSAGE, icon2);
		
	}

}
