package tui;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import domain.Correntistas;
import persistence.BancoDAO;

public class Sistema{
	static ImageIcon icon = new ImageIcon("Banco.png");
	static ImageIcon icon2 = new ImageIcon("iconBP.png");
	
	public static void main(String[] args) {
		intro();
		
	}
	
	public static void intro() {

			String[] comeco = { "Quero fazer parte do pov�o", "Vou procurar outro banco" };
			String introducao = "    Bem vindo ao seu Banco. O banco do pov�o! \n Nele voc� pode fazer depositos, saques, consulta\n de saldos entre outros,"
					+ " com a diferen�a taxas bem\n baixinhas e muita gente junto com voc�. \nCaso tenha certeza de que quer abrir uma conta � s� continuar.";
			int resp2 = JOptionPane.showOptionDialog(null, introducao, "Banco do Pov�o", JOptionPane.YES_NO_OPTION,
					JOptionPane.INFORMATION_MESSAGE, icon2, comeco, comeco[0]);
			if (resp2 == 0) {
				BancoTUI.abrirConta();
			}else {
				System.exit(0);
			}

			String[] conta = { "J� possuo uma conta", "Me enganei", "Quero abrir uma conta" };
			int resp = JOptionPane.showOptionDialog(null, null, "Banco do Pov�o", JOptionPane.YES_NO_OPTION,
					JOptionPane.INFORMATION_MESSAGE, icon, conta, conta[0]);
			if(resp == 0) {
				clienteConta();
			}else if (resp == 2) {
				intro();
			}else {
				System.exit(0);
			}
		

	}

	public static  void clienteConta() {
		String nome = JOptionPane.showInputDialog(icon2, "Por favor insira o nome completo do proprietario(a) da conta:");
		Correntistas correntista = BancoDAO.buscarConta(nome);
		acoesConta(correntista);
		
	}

	public static void acoesConta(Correntistas correntista) {
		

		
		String[] opcoes = {"Dep�sito", "Saque", "Consulta de Saldo", "Bloqueio de Conta", "Encerramento de conta", "Desbloqueio de conta", "Status da Conta", "Abrir outra conta", "Consultar outra conta", "Sair" };
		int resp = JOptionPane.showOptionDialog(null, "Escolha a a��o que deseja realizar: ", "Banco do Pov�o", JOptionPane.YES_NO_OPTION,
				JOptionPane.INFORMATION_MESSAGE, icon2, opcoes, opcoes[0]);
		switch(resp) {
		case 0:
			Double deposito = Double.parseDouble(JOptionPane.showInputDialog(icon2, "Insira o valor que deseja dep�sitar no formato XXX.XX: "));
			BancoDAO.deposito(deposito, correntista);
			acoesConta(correntista);
		case 1:
			Double saque = Double.parseDouble(JOptionPane.showInputDialog(icon2, "Insira o valor que deseja sacar no formato XXX.XX: "));
			BancoDAO.saque(saque, correntista);
			acoesConta(correntista);
		case 2:
			BancoDAO.consulta(correntista);
			acoesConta(correntista);
		case 3:
			BancoDAO.bloqueio(correntista);
			acoesConta(correntista);
		case 4:
			BancoDAO.encerramento(correntista);
			acoesConta(correntista);
		case 5:
			BancoDAO.desbloqueio(correntista);
			acoesConta(correntista);
		case 6:
			BancoDAO.statusConta(correntista);
			acoesConta(correntista);
		case 7:
			BancoTUI.abrirConta();
		case 8:
			clienteConta();
		case 9:
			System.exit(0);
		}
		
		
	}
	
	

}
