package tui;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import domain.Correntistas;
import enums.BancoEnum;
import persistence.BancoDAO;

public class BancoTUI {
	static ImageIcon icon = new ImageIcon("Banco.png");
	static ImageIcon icon2 = new ImageIcon("iconBP.png");
	static int cont;

	public static void abrirConta() {
		JOptionPane.showMessageDialog(null,
				"Para abrir uma conta voc� precisa desponibilizar alguns\n dados"
						+ " pessoais que estar�o guardados a \"sete-chaves\" com a gente.",
				"Banco do Pov�o", JOptionPane.INFORMATION_MESSAGE, icon2);

		String cpf = JOptionPane.showInputDialog(icon2, "CPF:");
		String nome = JOptionPane.showInputDialog(icon2, "Nome completo:");
		String telefone = JOptionPane.showInputDialog(icon2, "Telefone: (Formato: (XX) XXXXX-XXXX)");
		String endereco = JOptionPane.showInputDialog(icon2, "Endere�o:");
		double rendaM = Double.parseDouble(JOptionPane.showInputDialog(icon2, "Renda Mensal:"));
		double saldo = 0.00;
		double credito = 0.00;
		BancoEnum statusConta = BancoEnum.SUSPENSA;
        int numConta = cont + 1;
        cont = numConta;

		JOptionPane.showMessageDialog(null,
				"Estamos quase l�! Para voc� concluir \n"
						+ "o cadstro prescisamos que voc� complete \n mais algumas informa��es.\n (� rapidinho!)",
				"Banco do Pov�o", JOptionPane.INFORMATION_MESSAGE, icon2);
		String rg = JOptionPane.showInputDialog(icon2, "RG:");
		JOptionPane.showMessageDialog(null,
				"Agora para finaizar, precisamos que voc� envie para\n"
						+ "este email: bancodopovao@bancodopovao.com o comprovante de renda \n"
						+ "e o comprovante de resid�ncia do titular.",
				"Banco do Pov�o", JOptionPane.INFORMATION_MESSAGE, icon2);
		JOptionPane.showMessageDialog(null,
				"Cadastro concluido!\n Sua conta est� aberta e"
						+ " quase pronta para ser usada.\n S� precisamos validar os seus comprovantes",
				"Banco do Pov�o", JOptionPane.INFORMATION_MESSAGE, icon2);

		Correntistas correntista = new Correntistas(cpf, nome, telefone, endereco, rendaM, rg, saldo, credito,
				statusConta, numConta);
		BancoDAO.criarConta(correntista);
		statusConta(correntista);
		credito(correntista);
		Sistema.acoesConta(correntista);

	}

	public static void conta(Correntistas correntista) {

		JOptionPane.showMessageDialog(null,
				"Nome: " + correntista.getNome() + "\n" + "Telefone: " + correntista.getTelefone() + "\n" + "Cpf: "
						+ correntista.getCpf() + "\n" + "Endere�o: " + correntista.getEndereco() + "\n"
						+ "Renda Mensal: " + correntista.getRendaM() + "\n" + "Saldo: " + correntista.getSaldo() + "\n"
						+ "Credito: " + correntista.getCredito() + "\n" + "Status da Conta: "
						+ correntista.getStatusConta() + "\n" + "Numero da Conta: " + correntista.getNumConta(),
				"Sua Conta", JOptionPane.INFORMATION_MESSAGE, icon2);
		if (correntista.getStatusConta() != BancoEnum.ATIVA) {
			statusConta(correntista);
		}
		if (correntista.getCredito() == 0.0) {
			credito(correntista);
		}

	}

	private static void credito(Correntistas correntista) {
		int resp = JOptionPane.showConfirmDialog(null,
				"Ops, verificamos que sua conta se encontra com credito zerado. Gostaria de fazer uma analise para aumentar o seu credito?",
				"Banco do Pov�o", JOptionPane.YES_NO_OPTION, JOptionPane.YES_NO_OPTION, icon2);
		if (resp == 0) {
			correntista.setCredito(3000.00);
			JOptionPane.showMessageDialog(null,
					"Verificamos sua conta e agora voc� possui um valor equivalente a R$3.000,00 (tr�s mil reais), para gastar a vontade.",
					"Banco do Pov�o", JOptionPane.INFORMATION_MESSAGE, icon2);
		} else
			JOptionPane.showMessageDialog(null,
					"Caso mude de ideia voc� pode a qualquer momento pedir uma an�lise de cr�dito.", "Banco do Pov�o",
					JOptionPane.INFORMATION_MESSAGE, icon2);

	}

	private static void statusConta(Correntistas correntista) {
		if (correntista.getStatusConta() == BancoEnum.SUSPENSA) {
			int resp = JOptionPane.showConfirmDialog(null,
					"Ops, verificamos que sua conta se encontra SUSPENSA, voc� ja entregou os seus comprovantes?",
					"Banco do Pov�o", JOptionPane.YES_NO_OPTION, JOptionPane.YES_NO_OPTION, icon2);
			if (resp == 0) {
				correntista.setStatusConta(BancoEnum.ATIVA);
				JOptionPane.showMessageDialog(null, "OK, verificaremos em que parte n�s erramos.", "Banco do Pov�o",
						JOptionPane.INFORMATION_MESSAGE, icon2);
			}else
				JOptionPane.showMessageDialog(null,
						"Para poder utilizar a sua conta, precisamos que voc� nos envie os comprovantes, at� l� sua conta ir� continuar SUSPENSA.",
						"Banco do Pov�o", JOptionPane.INFORMATION_MESSAGE, icon2);
		} else if (correntista.getStatusConta() == BancoEnum.BLOQUEADA) {
			int resp = JOptionPane.showConfirmDialog(null,
					"Ops, verificamos que sua conta se encontra BLOQUEADA, gostaria de desbloquea-la?",
					"Banco do Pov�o", JOptionPane.YES_NO_OPTION, JOptionPane.YES_NO_OPTION, icon2);
			if (resp == 0)
				JOptionPane.showMessageDialog(null, "OK, iremos realizar os procedimentos para desbloquea-la.",
						"Banco do Pov�o", JOptionPane.INFORMATION_MESSAGE, icon2);
			else
				JOptionPane.showMessageDialog(null,
						"Ficamos tristes em saber que voc� deseja continuar com a sua conta bloqueada, mas a qualquer momento em que queira desbloquea-la basta ligar para 0000-0000.",
						"Banco do Pov�o", JOptionPane.INFORMATION_MESSAGE, icon2);
		} else if (correntista.getStatusConta() == BancoEnum.ENCERRADA) {
			int resp = JOptionPane.showConfirmDialog(null,
					"Ops, verificamos que sua conta se encontra ENCERRADA, gostaria de abrir uma nova conta?",
					"Banco do Pov�o", JOptionPane.YES_NO_OPTION, JOptionPane.YES_NO_OPTION, icon2);
			if (resp == 0)
				abrirConta();
			else
				JOptionPane.showMessageDialog(null,
						"Ficamos tristes em saber que voc� rompeu seu contrato com a nossa corpora��o, mas saiba que a qualquer momento voc� pode voltar a abrir uma nova conta no seu nome.",
						"Banco do Pov�o", JOptionPane.INFORMATION_MESSAGE, icon2);
		}

	}

}