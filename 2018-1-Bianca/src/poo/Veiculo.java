package poo;

class Veiculo {

	String placa;
	int anoFabricacao;
	TipoVeiculos tipo;
	
	Veiculo(String placa, int ano, TipoVeiculos tipo){
       this.placa = placa;
       this.anoFabricacao = ano;
       this.tipo = tipo;
	}
	
	
	
	private void setTipo(TipoVeiculos tipo) {
		this.tipo = tipo;
	}
	private String getPlaca() {
		return placa;
	}
	private int getAnoFabricacao() {
		return anoFabricacao;
	}
	private TipoVeiculos getTipo() {
		return tipo;
	}
	
	
}
