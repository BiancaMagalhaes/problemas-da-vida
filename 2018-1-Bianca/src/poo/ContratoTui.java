package poo;

import java.util.ArrayList;
import java.util.Scanner;

class ContratoTui {

	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.println("***************Contrato**************");
		System.out.println("Nome:");
		String nome = sc.next();
		System.out.println("Endere�o:");
		String endereco = sc.next();

		Contrato contrato = new Contrato(nome, endereco);

		System.out.println("Numero de veiculos que voc� deseja adicionar");
		int num = sc.nextInt();
		
		boolean resp1 = false;
		TipoVeiculos tipo = null;

		while (num != 0) {
			System.out.println("******Veiculo******");
			System.out.println("Placa:");
			String placa = sc.next();
			System.out.println("Ano de Fabrica��o:");
			int ano = sc.nextInt();
			System.out.println("Veiculo Adicionado");
			while(resp1 == false) {
			System.out.println("Tipo veiculo: " + TipoVeiculos.BASICO + " ou " + TipoVeiculos.INTERMEDIARIO + " ou "
					+ TipoVeiculos.LUXO);
			String tipoNome = sc.next();
			if (tipoNome.equals("BASICO")) {
				tipo = TipoVeiculos.BASICO;
				resp1 = true;
			} else if (tipoNome.equals("INTERMEDIARIO")) {
			    tipo = TipoVeiculos.INTERMEDIARIO;
				resp1 = true;			
			} else if (tipoNome.equals("LUXO")) {
				tipo = TipoVeiculos.LUXO;
			    resp1 = true;
			} else {
				System.out.println("Desculpe n�o reconheci, voc� poderia digitar novamente com letras maiusculas");
                resp1 = false;			
			}
			
			}
			
			Veiculo carro = new Veiculo(placa, ano, tipo);
			Contrato.adicionarVeiculo(carro);
			num--;
		}
		ArrayList Veiculos = Contrato.consultarVeiculos();
		for (int i = 0; i < Veiculos.size(); i++) {
			System.out.println("Veiculo " + i + " :\n" + "Placa: " + Contrato.lista.get(i).placa +
					"\nAno: " + Contrato.lista.get(i).anoFabricacao + "\nTipo: " + Contrato.lista.get(i).tipo);
		}
		
		boolean resp = false;
		while (resp == false) {
			System.out.println("****Remover Veiculo do Contrato******");
			System.out.println("Digite a placa do veiculo que deseja remover:");
			String plaquinha = sc.next();
			resp = Contrato.removerVeiculo(plaquinha);
			if (resp == true) {
				System.out.println("Veiculo removido com sucesso");
			} else {
				System.out.println("Veiculo n�o encontrado");
			}
		}
		
		Veiculos = Contrato.consultarVeiculos();
		for (int i = 0; i < Veiculos.size(); i++) {
			System.out.println("Veiculo " + i + " :\n" + "Placa: " + Contrato.lista.get(i).placa +
					"\nAno: " + Contrato.lista.get(i).anoFabricacao + "\nTipo: " + Contrato.lista.get(i).tipo);
		}

	}

}
