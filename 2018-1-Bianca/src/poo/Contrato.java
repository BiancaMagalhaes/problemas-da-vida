package poo;

import java.util.ArrayList;


class Contrato {

	int NumContrato = 0;
	String nome;
	String enderco;
	static ArrayList<Veiculo> lista = new ArrayList<Veiculo>();
	static Double valorTotal = 0.0;

	Contrato(String nome, String endereco) {
		this.nome = nome;
		this.enderco = endereco;
		this.NumContrato++;
	}

	private String getNome() {
		return nome;
	}

	private String getEnderco() {
		return enderco;
	}

	private Double getValorTotal() {
		return valorTotal;
	}

	static Boolean adicionarVeiculo(Veiculo veiculo) {
		if (lista.contains(veiculo.placa)) {
			return false;
		} else {
			if (veiculo.tipo == TipoVeiculos.BASICO) {
				valorTotal += 100.45;
			} else if (veiculo.tipo == TipoVeiculos.INTERMEDIARIO) {
				valorTotal += 130.10;
			} else {
				valorTotal += 156.00;
			}
			lista.add(veiculo);
			return true;
		}

	}

	static Boolean removerVeiculo(String placa) {
		for (int i = 0; i < lista.size(); i++) {
			if (lista.get(i).placa.equals(placa)) {
				lista.remove(i);
				if (lista.get(i).tipo == TipoVeiculos.BASICO) {
					valorTotal -= 100.45;
				} else if (lista.get(i).tipo == TipoVeiculos.INTERMEDIARIO) {
					valorTotal -= 130.10;
				} else {
					valorTotal -= 156.00;
				}
				return true;
			} else if (i == lista.size() -1) {
				return false;
			}

		}
		return null;
	}

	static ArrayList<Veiculo> consultarVeiculos() {
		
		return lista;
	}

	private ArrayList<Veiculo> consultarVeiculos(TipoVeiculos tipo) {
		ArrayList<Veiculo> porTipo = new ArrayList<Veiculo>();
		for (int i = 0; i < lista.size(); i++) {
			if (lista.get(i).tipo == tipo) {
				porTipo.addAll(lista);

			}
		}
		return porTipo;
	}

	private Veiculo consultarVeiculo(String placa) {
		for (int i = 0; i < lista.size(); i++) {
			if (lista.get(i).placa == placa) {
				return lista.get(i);
			}
		}
		return null;
	}

}
